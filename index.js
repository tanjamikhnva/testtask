function checkPromocode (promocode) {
    if (typeof promocode !== 'number') return
    
    let promArr = [];
    let odd = 0;
    let even = 0;
    let oddCounter = 0;  
    let pairCounter = 0;  
    let pairGrowthCounter = 0;  

    while(promocode > 0) {
        promArr.unshift(promocode % 10);
        promocode = promocode / 10 | 0;
    }
    if (promArr.length !== 8) return
    
    for (let i = 0; i < promArr.length; i++) {
        if (promArr[i] % 2 === 0) {
            even += promArr[i]
            if (oddCounter === 1) {  
                pairCounter = 0;  
                pairGrowthCounter = 0;  
                oddCounter = 0;  
              } else { 
                  oddCounter = 0; 
                }  
        } else {
            odd += promArr[i]
            if (++oddCounter === 2) {  
                if (promArr[i] > promArr[i - 1]) {  
                  if (++pairGrowthCounter === 2) { return 2000 };
                  if (++pairCounter === 2) { return 1000 };
                } else {  
                    pairGrowthCounter = 0;  
                  if (++pairCounter === 2) { return 1000 };  
                }  
            }  
        }  
    } 
    return (even > odd ? 100 : 0)
}
       
checkPromocode (84533920) //0
checkPromocode (48183276) // 100
checkPromocode (73289388) // 1000
checkPromocode (37283988) //2000